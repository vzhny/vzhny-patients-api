require('./config/config');

const express = require('express');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const cors = require('cors');
const { mongoose } = require('./db/mongoose');
const routes = require('./api/routes');

const app = express();
const port = process.env.PORT || 3000;
const endpoint = 'http://localhost:3000';

// Middleware
app.use(helmet());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(
  cors({
    origin: endpoint,
  })
);

// API routes
app.use('/api', routes);

// * Heroku Wakeup Route
app.get('/', (req, res) => {
  const message = 'Please go to https://gitlab.com/vzhny/patientex-api#documentation for API usage information.';
  res.status(200).send(message);
});

app.listen(port, () => {
  console.log(`The patientex API is running on ${endpoint}!`);
});
