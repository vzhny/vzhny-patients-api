const jwt = require('jsonwebtoken');

function verifyToken(req, res, next) {
  // Reading the current token stored in the browser's headers
  const token = req.headers['x-auth-token'];

  if (!token) {
    // If the token does not exist, return a 403 and set auth to false with an error message
    // @CHANGE - .send -> .json
    return res.status(403).json({
      auth: false,
      message: 'No token provided',
    });
  }

  // Verifying the token from the browser's headers
  jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
    if (err) {
      // If the token is invalid, return a 500 and set auth to false with an error message
      // @CHANGE - .send -> .json
      return res.status(500).json({
        auth: false,
        message: 'Failed to authenticate token',
      });
    }
    // Set the userId in the request to the decoded id to be used in the next router controller func
    req.userId = decoded.id;
  });
  next();
}

module.exports = verifyToken;
