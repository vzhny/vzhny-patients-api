const express = require('express');
const patientsController = require('./controllers/patients.controllers');
const authController = require('./controllers/auth.controllers');
const verifyToken = require('../auth/verify.token');

const router = express.Router();

// GET and POST patients routes
router
  .route('/patients')
  .get(verifyToken, patientsController.patientsGetAll)
  .post(verifyToken, patientsController.patientAddOne);

// GET, PUT, and DELETE patients/:id routes
router
  .route('/patients/:patientId')
  .get(verifyToken, patientsController.patientGetOne)
  .put(verifyToken, patientsController.patientUpdateOne)
  .delete(verifyToken, patientsController.patientDeleteOne);

// POST register route
router.route('/auth/register').post(authController.register);

// POST login route
router.route('/auth/login').post(authController.login);

// POST (explicit) logout route
router.route('/auth/logout').post(authController.logout);

module.exports = router;
