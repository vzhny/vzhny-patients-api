const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const User = mongoose.model('User');

// POST register route controller
module.exports.register = (req, res) => {
  // Initializing variables for readability
  const { username, name, password } = req.body;

  User.create(
    {
      username,
      name,
      password: bcrypt.hashSync(password, 10),
    },
    (err, user) => {
      // If there is an error, send a 400 and the error message
      if (err) {
        return res.status(400).json({
          message: `There was an error registering ${username}`,
        });
      }
      // If there is no error, sign a token, send a 201 and enable auth with the token;
      // the token expires in 24 hours
      const token = jwt.sign({ id: user._id }, process.env.JWT_SECRET, { expiresIn: 86400 });

      // @CHANGE - .send --> .json
      return res.status(201).json({
        auth: true,
        token,
      });
    }
  );
};

// POST login route controller
module.exports.login = (req, res) => {
  // Initializing consts for readability
  const { username, password } = req.body;

  User.findOne({ username }).exec((err, user) => {
    if (err) {
      // If there is an error, send a 400 and the error message
      return res.status(400).json({
        message: `There was an error logging in ${username}`,
      });
    }
    if (!user) {
      // If the user could not be found, send a 404 and the error message
      return res.status(404).json({
        message: 'Could not find user/wrong password',
      });
    }
    if (bcrypt.compareSync(password, user.password)) {
      // If there is no error, sign a token, send a 200 and enable auth with the token;
      // the token expires in 24 hours
      const token = jwt.sign({ id: user._id }, process.env.JWT_SECRET, { expiresIn: 86400 });

      // @CHANGE - .send --> .json
      return res.status(200).json({
        auth: true,
        token,
      });
    }
    // If there was an unauthorized login attempt, send a 401 and the error message
    return res.status(401).json({
      message: 'Unauthorized login attempt',
    });
  });
};

// POST logout route controller
module.exports.logout = (req, res) => {
  // If the user wishes to log out, send a 200 and set their auth to false and token to null
  // @CHANGE - .send --> .json
  res.status(200).json({
    auth: false,
    token: null,
  });
};
